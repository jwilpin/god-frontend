'use strict';

/**
 * @ngdoc function
 * @name godFrontendApp.controller:StartCtrl
 * @description
 * # StartCtrl
 * Controller of the godFrontendApp
 */
angular.module('godFrontendApp').controller('StartCtrl', ['Rules', 'Game', '$location', function (Rules, Game, $location) {
        var vm = this;
        
        vm.player1 = "";
        vm.player2 = "";
        
        vm.startGame = function(){
            Rules.getRules().then(
                function(rules) {
                    Game.start(rules, vm.player1, vm.player2);
                    $location.path('/play');
                }, function(error) {
                    vm.errorMessage = "Can´t start game, try again please!";
                    console.log(error);
                });
        };    
        
}]);
