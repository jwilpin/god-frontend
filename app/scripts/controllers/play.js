'use strict';

/**
 * @ngdoc function
 * @name godFrontendApp.controller:PlayCtrl
 * @description
 * # PlayCtrl
 * Controller of the godFrontendApp
 */
angular.module('godFrontendApp').controller('PlayCtrl', ['Game', '$location', function (Game, $location) {
   if(!Game.isStarted()){
        $location.path('/start');
   }
   
   var vm = this;
   
   vm.moves = Game.rules.moves;
   vm.round = Game.getRound();
   vm.player = Game.getPlayerTurn();
   
   vm.setPlayerMove = function(){
       Game.addMove(JSON.parse(vm.playerMove));
       // check game finished
       if(Game.winner){
            $location.path('/finish');
       } else {
            vm.round = Game.getRound();
            vm.player = Game.getPlayerTurn();
            vm.playerMove = {};
       }
   }
    
}]);
