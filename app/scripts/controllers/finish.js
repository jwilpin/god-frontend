'use strict';

/**
 * @ngdoc function
 * @name godFrontendApp.controller:FinishCtrl
 * @description
 * # FinishCtrl
 * Controller of the godFrontendApp
 */
angular.module('godFrontendApp').controller('FinishCtrl', ['Game', 'Record', '$location', function (Game, Record, $location) {
    if(!Game.isStarted()){
        $location.path('/start');
    }
    
    var vm = this;
   
    vm.winner = Game.winner;
    vm.start = function(){
        Record.save(vm.winner).then(
            function() {
                $location.path('/start');
            }, function(error) {
                vm.errorMessage = "Can´t save your victory";
            });
    }
   
}]);
