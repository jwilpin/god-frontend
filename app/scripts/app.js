'use strict';

/**
 * @ngdoc overview
 * @name godFrontendApp
 * @description
 * # godFrontendApp
 *
 * Main module of the application.
 */
angular.module('godFrontendApp', ['config', 'ngRoute', 'ngResource', 'restangular']).config(function ($routeProvider) {
    $routeProvider
      .when('/start', {
        templateUrl: 'views/start.html',
        controller: 'StartCtrl',
        controllerAs: 'start'
      })
      .when('/play', {
        templateUrl: 'views/play.html',
        controller: 'PlayCtrl',
        controllerAs: 'play'
      })
      .when('/finish', {
        templateUrl: 'views/finish.html',
        controller: 'FinishCtrl',
        controllerAs: 'finish'
      })
      .otherwise({
        redirectTo: '/start'
      });
  });

angular.module('godFrontendApp').config(function (ENV, RestangularProvider) {
    RestangularProvider.setBaseUrl(ENV.apiEndpoint);
    RestangularProvider.setDefaultHeaders({'Access-Control-Allow-Origin': 'false'});
});