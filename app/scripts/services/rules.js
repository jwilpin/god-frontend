'use strict';

/**
 * @ngdoc service
 * @name godFrontendApp.rules
 * @description
 * # rules
 * Service in the godFrontendApp.
 */
angular.module('godFrontendApp').service('Rules', ['Restangular', function (Restangular) {
    
    var Rules = Restangular.one('rules');
    
    this.getRules = function(){
        return Rules.get({});
    }
    
  }]);
