'use strict';

/**
 * @ngdoc service
 * @name godFrontendApp.game
 * @description
 * # game
 * Factory in the godFrontendApp.
 */
angular.module('godFrontendApp').factory('Game', function () {
    
    var game = this;
    var self = this;
    
    var rules = game.rules = {};
    var players = game.players = [];
    var moves = game.moves = [];
    var beats = game.beats = [];
    
    game.start = function(_rules, player1, player2) {
        rules = self.rules = _rules;
        players = self.players = [];
        self.players.push({name: player1, wins: 0});
        self.players.push({name: player2, wins: 0});
        moves = self.moves = [];
        beats = self.beats = [];
        self.winner = undefined;
    };
    
    game.isStarted = function(){
      return players.length > 0;  
    };

    game.getRound = function(){
        return Math.floor(moves.length / 2) + 1;
    };
    
    game.getPlayerTurn = function() {
        return players[moves.length % 2].name;
    };
    
    game.addMove = function(move) {
        moves.push(move);
        var round = self.getRound();
        if(moves.length % 2 === 0){
            var movePlayer1 = moves[moves.length - 2];
            var movePlayer2 = moves[moves.length - 1];
            // player 1 wins?
            if(movePlayer1.kills === movePlayer2.move){
                beats.push({round: round, player: players[0].name});
                players[0].wins++;
            } 
            // player 2 wins?
            else if(movePlayer2.kills === movePlayer1.move){
                beats.push({round: round, player: players[1].name});
                players[1].wins++;
            } 
            // tie   
            else {
                beats.push({round: round, player: "Tie"});
            }
            // we have a winner?
            if(players[0].wins === rules.beatsToWin) self.winner = players[0].name;
            if(players[1].wins === rules.beatsToWin) self.winner = players[1].name;
        }
    };
    
    return game;
});
