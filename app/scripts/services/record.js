'use strict';

/**
 * @ngdoc service
 * @name godFrontendApp.record
 * @description
 * # record
 * Service in the godFrontendApp.
 */
angular.module('godFrontendApp').service('Record', ['Restangular', function (Restangular) {
    
    var ScoreRecords = Restangular.all('scorerecords');
    
    this.list = function(){
        return ScoreRecords.getList();
    }
    
    this.save = function(player){
        return ScoreRecords.post({player: player, wins: 1});
    }
    
  }]);
