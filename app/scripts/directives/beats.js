'use strict';

/**
 * @ngdoc directive
 * @name godFrontendApp.directive:beats
 * @description
 * # beats
 */
angular.module('godFrontendApp').directive('beats', ['Game', function (Game) {
    return {
      templateUrl: 'views/beats.html',
      restrict: 'E',
      link: function postLink($scope, element, attrs) {
        $scope.beats = Game.beats;
      }
    };
  }]);
