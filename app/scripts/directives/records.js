'use strict';

/**
 * @ngdoc directive
 * @name godFrontendApp.directive:records
 * @description
 * # records
 */
angular.module('godFrontendApp').directive('records', ['Record', function (Record) {
    return {
      templateUrl: '/views/records.html',
      restrict: 'E',
      link: function postLink($scope, element, attrs) {
        Record.list().then(
            function(records) {
                $scope.records = records;
            }, function(error) {
                $scope.errorMessage = "Can´t load player records";
        });
      }
    };
  }]);
