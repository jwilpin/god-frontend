'use strict';

describe('Directive: beats', function () {

  // load the directive's module
  beforeEach(module('godFrontendApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<beats></beats>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the beats directive');
  }));
});
